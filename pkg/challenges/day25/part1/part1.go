package part1

import "errors"

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	err = errors.New("this part is not yet implemented")

	return
}
