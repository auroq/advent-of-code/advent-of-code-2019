package part2

import (
	"strconv"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []struct {
		lines    []string
		expected int
	}{
		{[]string{
			"157 ORE => 5 NZVS",
			"165 ORE => 6 DCFZ",
			"44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL",
			"12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ",
			"179 ORE => 7 PSHF",
			"177 ORE => 5 HKGWZ",
			"7 DCFZ, 7 PSHF => 2 XJWVT",
			"165 ORE => 2 GPVTF",
			"3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT",
		}, 82892753},
	}
	for _, input := range inputs {
		t.Run(strconv.Itoa(input.expected), func(t *testing.T) {
			expected := input.expected
			actual, err := Part2{}.Run(input.lines)
			if err != nil {
				t.Errorf("error: %v", err)
			}
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestGetOre(t *testing.T) {
	inputs := []string{
		"10 ORE => 10 A",
		"1 ORE => 1 B",
		"7 A, 1 B => 1 C",
		"7 A, 1 C => 1 D",
		"7 A, 1 D => 1 E",
		"7 A, 1 E => 1 FUEL",
	}
	reqMap, err := parseInput(inputs)
	if err != nil {
		t.Fatal(err)
	}
	var testInputs = []struct {
		resourceName string
		ore          int
	}{
		{"A", 10},
		{"B", 1},
		{"C", 11},
		{"D", 21},
		{"E", 31},
		{"FUEL", 31},
	}
	for _, input := range testInputs {
		t.Run(input.resourceName, func(t *testing.T) {
			expected := input.ore
			actual := getOre(Resource{input.resourceName, 1}, reqMap, &map[string]int{})
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestConvert(t *testing.T) {
	var inputs = []struct {
		amount    int
		req       Requirement
		waste     Resource
		resources []Resource
	}{
		{
			7,
			Requirement{
				Resource{"A", 10},
				[]Resource{
					{"ORE", 10},
				},
			},
			Resource{"A", 3},
			[]Resource{{"ORE", 10}},
		}, {
			10,
			Requirement{
				Resource{"A", 10},
				[]Resource{
					{"ORE", 10},
				},
			},
			Resource{},
			[]Resource{{"ORE", 10}},
		}, {
			17,
			Requirement{
				Resource{"A", 10},
				[]Resource{
					{"ORE", 10},
				},
			},
			Resource{"A", 3},
			[]Resource{{"ORE", 20}},
		}, {
			17,
			Requirement{
				Resource{"A", 10},
				[]Resource{
					{"ORE", 10},
					{"B", 5},
				},
			},
			Resource{"A", 3},
			[]Resource{
				{"ORE", 20},
				{"B", 10},
			},
		},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {

			waste, resources := convert(input.amount, input.req)
			for i := range resources {
				if resources[i] != input.resources[i] {
					t.Fatalf("expected: %v, actual: %v", input.resources[i], resources[i])
				}
			}
			if waste != input.waste {
				t.Fatalf("expected: %v, actual: %v", input.waste, waste)
			}
		})
	}
}

func TestParseLine(t *testing.T) {
	var inputs = []struct {
		line   string
		inputs []Resource
		output Resource
	}{
		{
			"10 ORE => 10 A",
			[]Resource{{"ORE", 10}},
			Resource{"A", 10},
		}, {
			"1 ORE => 1 B",
			[]Resource{{"ORE", 1}},
			Resource{"B", 1},
		}, {
			"7 A, 1 B => 1 C",
			[]Resource{{"A", 7}, {"B", 1}},
			Resource{"C", 1},
		}, {
			"7 A, 1 C => 1 D",
			[]Resource{{"A", 7}, {"C", 1}},
			Resource{"D", 1},
		}, {
			"7 A, 1 D => 1 E",
			[]Resource{{"A", 7}, {"D", 1}},
			Resource{"E", 1},
		}, {
			"7 A, 1 E => 1 FUEL",
			[]Resource{{"A", 7}, {"E", 1}},
			Resource{"FUEL", 1},
		},
	}
	for _, input := range inputs {
		t.Run(input.output.name, func(t *testing.T) {
			inputResources, output, err := parseLine(input.line)
			if err != nil {
				t.Fatalf("error: %v", err)
			}
			for i := range inputResources {
				if inputResources[i] != input.inputs[i] {
					t.Fatalf("expected: %v, actual: %v", input.inputs[i], inputResources[i])
				}
			}
			if output != input.output {
				t.Fatalf("expected: %v, actual: %v", input.output, output)
			}
		})
	}
}
