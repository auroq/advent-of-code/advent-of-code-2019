package part2

import (
	"fmt"
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"strings"
	"sync"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	var state []int
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	inputInstruction := 5

	output := make(chan int)
	program := intcode.NewProgram([]int{inputInstruction}, state)
	program.SetFinalOutputConfiguration(&output)
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		for out := range output {
			if out == 0 {
				fmt.Println(out)
			} else {
				answer = out
			}
		}
		wg.Done()
	}()
	program.Run()
	program.Close()
	close(output)
	wg.Wait()

	return
}
