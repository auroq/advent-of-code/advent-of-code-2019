package part2

import (
	"testing"
)

func TestPart2_Run(t *testing.T) {
	input := []string{
		"COM)B",
		"B)C",
		"C)D",
		"D)E",
		"E)F",
		"B)G",
		"G)H",
		"D)I",
		"E)J",
		"J)K",
		"K)L",
		"K)YOU",
		"I)SAN",
	}
	actual, err := Part2{}.Run(input)
	expected := 4
	if err != nil {
		t.Errorf("error thrown: %v", err)
	}
	if actual != expected {
		t.Errorf("expected: %d, actual: %d", expected, actual)
	}
}
