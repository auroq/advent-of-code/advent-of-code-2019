package part2

import (
	"strconv"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []struct {
		wire1    string
		wire2    string
		expected int
	}{
		{"R8,U5,L5,D3", "U7,R6,D4,L4", 30},
		{"R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 610},
		{"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 410},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.expected
			actual, err := Part2{}.Run([]string{input.wire1, input.wire2})
			if err != nil {
				t.Errorf("error occurred: %v", err)
			}
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestGetSteps(t *testing.T) {
	var inputs = []struct {
		wire     []Position
		name     string
		pos      Position
		expected int
	}{
		{
			[]Position{{1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {8, 0}, {8, 1}, {8, 2}, {8, 3}, {8, 4}, {8, 5}, {7, 5}, {6, 5}, {5, 5}, {4, 5}, {3, 5}, {3, 4}, {3, 3}, {3, 2}},
			"R8,U5,L5,D3", Position{3, 3}, 20,
		},
		{
			[]Position{{0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7}, {1, 7}, {2, 7}, {3, 7}, {4, 7}, {5, 7}, {6, 7}, {6, 6}, {6, 5}, {6, 4}, {6, 3}, {5, 3}, {4, 3}, {3, 3}, {2, 3}},
			"U7,R6,D4,L4", Position{3, 3}, 20,
		},
		{
			[]Position{{1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {8, 0}, {8, 1}, {8, 2}, {8, 3}, {8, 4}, {8, 5}, {7, 5}, {6, 5}, {5, 5}, {4, 5}, {3, 5}, {3, 4}, {3, 3}, {3, 2}},
			"R8,U5,L5,D3", Position{6, 5}, 15,
		},
		{
			[]Position{{0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7}, {1, 7}, {2, 7}, {3, 7}, {4, 7}, {5, 7}, {6, 7}, {6, 6}, {6, 5}, {6, 4}, {6, 3}, {5, 3}, {4, 3}, {3, 3}, {2, 3}},
			"U7,R6,D4,L4", Position{6, 5}, 15,
		},
	}
	for _, input := range inputs {
		t.Run(input.name, func(t *testing.T) {
			expected := input.expected
			actual, err := GetSteps(input.wire, input.pos)
			if err != nil {
				t.Errorf("error occurred: %v", err)
			}
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestMove(t *testing.T) {
	var inputs = []struct {
		movement string
		expected []Position
	}{
		{"U2", []Position{
			{0, 1},
			{0, 2},
		}},
		{"D2", []Position{
			{0, -1},
			{0, -2},
		}},
		{"L2", []Position{
			{-1, 0},
			{-2, 0},
		}},
		{"R2", []Position{
			{1, 0},
			{2, 0},
		}},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			pos := Position{0, 0}
			actual, err := Move(pos, input.movement)
			if err != nil {
				t.Errorf("error occurred: %v", err)
			}
			for i, pos := range actual {
				if pos.x != input.expected[i].x || pos.y != input.expected[i].y {
					t.Fatalf("expected: %v, actual: %v", input.expected, actual)
				}
			}
		})
	}
}

func TestGetDistance(t *testing.T) {
	var inputs = []struct {
		pos1     Position
		pos2     Position
		expected int
	}{
		{Position{0, 0}, Position{3, 3}, 6},
		{Position{8, 15}, Position{40, 37}, 54},
		{Position{-40, -20}, Position{60, 20}, 140},
		{Position{-80, -40}, Position{-40, -80}, 80},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			actual := GetDistance(input.pos1, input.pos2)
			if actual != input.expected {
				t.Fatalf("expected: %d, actual: %d", input.expected, actual)
			}
		})
	}
}
