package part2

import (
	"strconv"
	"testing"
)

func TestValidate(t *testing.T) {
	var inputs = []struct {
		num      string
		expected bool
	}{
		{"111122", true},
		{"111111", false},
		{"223450", false},
		{"123789", false},
	}
	for _, input := range inputs {
		t.Run(input.num, func(t *testing.T) {
			expected := input.expected
			actual, err := Validate(input.num)
			if err != nil {
				t.Errorf("error: %v", err)
			}
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}

func TestContainsAdjacent(t *testing.T) {
	var inputs = []struct {
		num      string
		expected bool
	}{
		{"22", true},
		{"12234", true},
		{"112233", true},
		{"123444", false},
		{"111122", true},
		{"12345", false},
	}
	for _, input := range inputs {
		t.Run(input.num, func(t *testing.T) {
			expected := input.expected
			actual := ContainsAdjacent(input.num)
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}

func TestDontDecrease(t *testing.T) {
	var inputs = []struct {
		num      string
		expected bool
	}{
		{"111123", true},
		{"135679", true},
		{"111111", true},
		{"223450", false},
		{"223245", false},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.expected
			actual, err := DoesntDecrease(input.num)
			if err != nil {
				t.Errorf("error: %v", err)
			}
			if actual != expected {
				t.Fatalf("expected: %t, actual: %t", expected, actual)
			}
		})
	}
}
