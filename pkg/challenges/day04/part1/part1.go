package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	answer = 0
	in := strings.Split(input[0], "-")
	min, err := strconv.Atoi(in[0])
	if err != nil {
		return -1, err
	}
	max, err := strconv.Atoi(in[1])
	if err != nil {
		return -1, err
	}
	for i := min; i <= max; i++ {
		valid, err := Validate(strconv.Itoa(i))
		if err != nil {
			return -1, err
		}
		if valid {
			answer++
		}
	}
	return
}

func Validate(num string) (valid bool, err error) {
	valid = true
	valid = ContainsAdjacent(num)
	if !valid {
		return
	}

	valid, err = DoesntDecrease(num)
	return
}

func ContainsAdjacent(num string) bool {
	for i := 0; i < len(num)-1; i++ {
		if num[i] == num[i+1] {
			return true
		}
	}
	return false
}

func DoesntDecrease(num string) (bool, error) {

	for i := 0; i < len(num)-1; i++ {
		num0, err := strconv.Atoi(num[i : i+1])
		if err != nil {
			return false, err
		}
		num1, err := strconv.Atoi(num[i+1 : i+2])
		if err != nil {
			return false, err
		}
		if num1 < num0 {
			return false, nil
		}
	}
	return true, nil
}
