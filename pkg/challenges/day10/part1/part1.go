package part1

import (
	"fmt"
	"math"
)

type Part1 struct{}

type Layer [][]int

type Asteroid struct {
	row, col int
}

type Grid []Asteroid

type RiseRun struct {
	rise, run int
}

type AsteroidSet []Asteroid

func (set *AsteroidSet) Add(asteroid Asteroid) {
	for _, a := range *set {
		if a.equals(asteroid) {
			return
		}
	}
	*set = append(*set, asteroid)
}

type RiseRunSet []RiseRun

func (set *RiseRunSet) Add(riseRun RiseRun) {
	for _, r := range *set {
		if r.equals(riseRun) {
			return
		}
	}
	*set = append(*set, riseRun)
}

func (part Part1) Run(input []string) (answer int, err error) {
	grid := parseInput(input)

	var minAst Asteroid
	max := math.MinInt64
	for _, a := range grid {
		count := a.seeCount(grid)
		if count > max {
			max = count
			minAst = a
		}
	}
	fmt.Printf("%v\n", minAst)
	answer = max

	return
}

func parseInput(input []string) (grid Grid) {
	for rowIndex, row := range input {
		for colIndex, col := range row {
			if col == '#' {
				grid = append(grid, Asteroid{rowIndex, colIndex})
			}
		}
	}

	return
}

func (asteroid Asteroid) equals(other Asteroid) bool {
	return asteroid.col == other.col && asteroid.row == other.row
}

func (riseRun RiseRun) equals(other RiseRun) bool {
	return riseRun.rise == other.rise && riseRun.run == other.run
}

func (asteroid Asteroid) seeCount(asteroids Grid) int {
	riseRunSet := RiseRunSet{}
	for _, a := range asteroids {
		if a.equals(asteroid) {
			continue
		}
		proportion := asteroid.getDistanceProportion(a)
		riseRunSet.Add(proportion)
	}

	return len(riseRunSet)
}

func (asteroid Asteroid) getDistanceProportion(other Asteroid) RiseRun {
	x := other.col - asteroid.col
	y := other.row - asteroid.row

	if abs(x) == abs(y) {
		if x < 1 {
			x = -1
		} else {
			x = 1
		}
		if y < 1 {
			y = -1
		} else {
			y = 1
		}
		return RiseRun{y, x}
	}

	if x == 0 {
		if y < 1 {
			return RiseRun{-1, 0}
		} else {
			return RiseRun{1, 0}
		}
	}

	if y == 0 {
		if x < 1 {
			return RiseRun{0, -1}
		} else {
			return RiseRun{0, 1}
		}
	}

	factor := greatestFactor(x, y)
	if factor == 1 || factor == -1 {
		return RiseRun{y, x}
	}

	xSign := 1
	ySign := 1
	if x < 1 {
		xSign = -1
	}
	if y < 1 {
		ySign = -1
	}
	return RiseRun{ySign * (y / factor), xSign * (x / factor)}
}

func abs(n int) int {
	if n < 1 {
		return n * -1
	}
	return n
}

func greatestFactor(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}
