package part2

import (
	"testing"
)

func TestPart2_Run(t *testing.T) {
	t.Skip()
	input := []string{"3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"}
	expected := 139629729
	actual, err := Part2{}.Run(input)
	if err != nil {
		t.Errorf("error occurred %v", err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
