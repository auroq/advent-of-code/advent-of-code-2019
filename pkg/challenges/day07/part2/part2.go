package part2

import (
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/utilities"
	"math"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	var state []int
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	perms := utilities.Permutations(5, 9)
	max := math.MinInt64
	for _, perm := range perms {
		program := intcode.NewProgram(perm, state)
		program.SetFinalOutputConfiguration(program.Computers[0].InChannel)
		program.Run()
		out := <-*program.Computers[0].InChannel
		if out > max {
			max = out
		}
	}

	answer = max
	return
}
