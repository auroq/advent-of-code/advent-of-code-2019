package part2

import (
	"strconv"
	"testing"
)

func TestPart2GetFuel(t *testing.T) {
	var inputs = []struct {
		input    int
		expected int
	}{
		{14, 2},
		{1969, 966},
		{100756, 50346},
	}
	for _, input := range inputs {
		t.Run(strconv.Itoa(input.input), func(t *testing.T) {
			if actual := GetFuel(input.input); actual != input.expected {
				t.Fatalf("expected: %d, actual: %d", input.expected, actual)
			}
		})
	}
}

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"14",
		"1969",
		"100756",
	}
	expected := 51314
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
