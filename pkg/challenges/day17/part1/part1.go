package part1

import (
	"fmt"
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"strings"
)

type Position struct {
	row, col int
}

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	karta, err := getMap(input[0])
	if err != nil {
		return
	}

	intersections := getIntersections(strings.Split(karta, "\n"))

	for _, intersection := range intersections {
		answer += intersection.getAlignmentParam()
	}

	fmt.Println(karta)
	return
}

func (pos Position) getAlignmentParam() int {
	return pos.row * pos.col
}

func getIntersections(karta []string) (positions []Position) {
	for row, rowVal := range karta {
		for col := range rowVal {
			if row == 0 || col == 0 || row == len(karta)-1 || col == len(rowVal)-1 {
				continue
			}
			if IsIntersection(row, col, karta) {
				positions = append(positions, Position{row, col})
			}
		}
	}

	return
}

func IsIntersection(row, col int, karta []string) bool {
	for _, val := range []uint8{
		karta[row][col],
		karta[row-1][col],
		karta[row][col-1],
		karta[row][col+1],
		karta[row+1][col],
	} {
		if val != '#' {
			return false
		}
	}

	return true
}

func getMap(input string) (karta string, err error) {

	var state []int
	stateStrs := strings.Split(input, ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	channel := make(chan int)
	program := intcode.NewProgram([]int{0}, state)
	program.SetFinalOutputConfiguration(&channel)

	go func() {
		for out := range channel {
			switch out {
			case 35:
				karta += "#"
			case 46:
				karta += "."
			case 10:
				karta += "\n"
			}
		}
	}()

	program.Run()
	close(channel)

	karta = strings.TrimRight(karta, "\n")

	return
}
