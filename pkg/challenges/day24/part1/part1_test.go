package part1

import "testing"

func TestGrid_Rating(t *testing.T) {
	grid := NewGrid([]string{
		".....",
		".....",
		".....",
		"#....",
		".#...",
	})
	expected := 2129920
	actual := grid.Rating()

	if actual != expected {
		t.Fatalf("expected: '%d', actual '%d'", expected, actual)
	}
}

func TestGrid_Equals(t *testing.T) {
	grid := NewGrid([]string{
		".....",
		".....",
		".....",
		"#....",
		".#...",
	})
	otherGrid := NewGrid([]string{
		".....",
		".....",
		".....",
		"#....",
		".#...",
	})
	if !grid.Equals(otherGrid) {
		t.Fatal("grids were expected to be equal but were not")
	}
}
