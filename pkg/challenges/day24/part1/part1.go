package part1

import (
	"fmt"
	"strconv"
)

type Part1 struct{}

type Position struct {
	row, col int
}

type Cell struct {
	position Position
	alive    bool
	Grid     *Grid
}

type Grid map[string]*Cell

func (part Part1) Run(input []string) (answer int, err error) {
	grid := NewGrid(input)
	fmt.Println(grid.toString())
	rating := grid.Rating()
	grids := map[int][]*Grid{}
	grids[rating] = []*Grid{&grid}

	for {
		grid = grid.step()
		fmt.Println(grid.toString())
		rating = grid.Rating()
		if gridSlice, ok := grids[rating]; ok {
			for _, other := range gridSlice {
				if other.Equals(grid) {
					return rating, nil
				}
			}
			grids[rating] = append(grids[rating], &grid)
		}
		grids[rating] = []*Grid{&grid}
	}
}

func (grid Grid) Rating() (rating int) {
	power := 1

	for row := 0; row < 5; row++ {
		for col := 0; col < 5; col++ {
			cell := grid[Position{row, col}.toString()]
			if cell.alive {
				rating += power
			}
			power = power * 2
		}
	}

	return
}

func (grid Grid) toString() (str string) {
	for row := 0; row < 5; row++ {
		for col := 0; col < 5; col++ {
			cell := grid[Position{row, col}.toString()]
			if cell.alive {
				str += "#"
			} else {
				str += "."
			}
		}
		str += "\n"
	}

	return
}

func NewGrid(input []string) (grid Grid) {
	grid = Grid{}
	for row, rowVal := range input {
		for col, symbol := range rowVal {
			var alive bool
			if symbol == '#' {
				alive = true
			}
			position := Position{row, col}
			grid[position.toString()] = &Cell{
				position: position,
				alive:    alive,
				Grid:     &grid,
			}
		}
	}

	return
}

func (cell Cell) step(neighborCount int, grid *Grid) (newCell Cell) {
	var alive bool

	if cell.alive {
		if neighborCount == 1 {
			alive = true
		} else {
			alive = false
		}
	} else {
		if neighborCount == 1 || neighborCount == 2 {
			alive = true
		} else {
			alive = cell.alive
		}
	}

	return Cell{
		position: cell.position,
		alive:    alive,
		Grid:     grid,
	}
}

func (grid Grid) step() (newGrid Grid) {
	newGrid = Grid{}
	for key, cell := range grid {
		neighbors := cell.getNeighborCount()
		newCell := cell.step(neighbors, &newGrid)
		newGrid[key] = &newCell
	}

	return
}

func (cell Cell) getNeighborCount() (count int) {
	for _, neighbor := range []Position{
		{cell.position.row - 1, cell.position.col},
		{cell.position.row, cell.position.col - 1},
		{cell.position.row, cell.position.col + 1},
		{cell.position.row + 1, cell.position.col},
	} {
		if cell.Grid.alive(neighbor) {
			count++
		}
	}

	return
}

func (grid Grid) alive(position Position) bool {
	if cell, ok := grid[position.toString()]; ok {
		return cell.alive
	}

	return false
}

func (pos Position) toString() string {
	return strconv.Itoa(pos.row) + "," + strconv.Itoa(pos.col)
}

func (pos Position) equals(other Position) bool {
	return pos.row == other.row &&
		pos.col == other.col
}

func (cell Cell) equals(other Cell) bool {
	return cell.position.equals(other.position) &&
		cell.alive == other.alive
}

func (grid Grid) Equals(other Grid) bool {
	for key, cell := range grid {
		if otherCell, ok := other[key]; !(ok && otherCell.equals(*cell)) {
			return false
		}
	}

	return true
}
