package part1

import (
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"strconv"
	"strings"
	"sync"
)

type Part1 struct{}

type TileType int

const (
	Empty      TileType = iota
	Wall       TileType = iota
	Block      TileType = iota
	Horizontal TileType = iota
	Ball       TileType = iota
)

type Tile struct {
	x, y     int
	tileType TileType
}

func (part Part1) Run(input []string) (answer int, err error) {
	var state intcode.State
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	channel := make(chan int)
	program := intcode.NewProgram([]int{1}, state)
	program.SetFinalOutputConfiguration(&channel)
	tiles := map[string]Tile{}
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		var x, y, tileId int
		index := 0
		for val := range channel {
			if index == 0 {
				x = val
				index++
			} else if index == 1 {
				y = val
				index++
			} else {
				tileId = val
				tiles[xyToString(x, y)] = Tile{
					x,
					y,
					ItoTileType(tileId),
				}
				index = 0
			}
		}
		wg.Done()
	}()
	program.Run()
	program.Close()
	close(channel)
	wg.Wait()

	for _, tile := range tiles {
		if tile.tileType == Block {
			answer++
		}
	}

	return
}

func xyToString(x, y int) string {
	return strconv.Itoa(x) + "," + strconv.Itoa(y)
}

func ItoTileType(i int) TileType {
	//0 is an empty tile. No game object appears in this tile.
	//1 is a wall tile. Walls are indestructible barriers.
	//2 is a block tile. Blocks can be broken by the ball.
	//3 is a horizontal paddle tile. The paddle is indestructible.
	//4 is a ball tile. The ball moves diag

	switch i {
	case 0:
		return Empty
	case 1:
		return Wall
	case 2:
		return Block
	case 3:
		return Horizontal
	case 4:
		return Ball
	default:
		return Empty
	}
}
