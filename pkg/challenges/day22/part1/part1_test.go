package part1

import (
	"strconv"
	"testing"
)

type testParams struct {
	initial  []int
	n        int
	expected []int
}

func TestShuffle(t *testing.T) {
	tests := []struct {
		commands []string
		expected []int
	}{
		{
			[]string{
				"deal with increment 7",
				"deal into new stack",
				"deal into new stack",
			}, []int{0, 3, 6, 9, 2, 5, 8, 1, 4, 7},
		}, {
			[]string{
				"cut 6",
				"deal with increment 7",
				"deal into new stack",
			}, []int{3, 0, 7, 4, 1, 8, 5, 2, 9, 6},
		}, {
			[]string{
				"deal with increment 7",
				"deal with increment 9",
				"cut -2",
			}, []int{6, 3, 0, 7, 4, 1, 8, 5, 2, 9},
		}, {
			[]string{
				"deal into new stack",
				"cut -2",
				"deal with increment 7",
				"cut 8",
				"cut -4",
				"deal with increment 7",
				"cut 3",
				"deal with increment 9",
				"deal with increment 3",
				"cut -1",
			}, []int{9, 2, 5, 8, 1, 4, 7, 0, 3, 6},
		},
	}
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := test.expected
			actual, err := Shuffle(test.commands, 10)
			if err != nil {
				t.Error(err)
			}
			for i := range expected {
				if actual[i] != expected[i] {
					t.Fatalf("expected: '%v', actual '%v'", expected, actual)
				}
			}
		})
	}
}

func TestDealIntoNewStack(t *testing.T) {
	tests := []testParams{
		{
			[]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
			10,
			[]int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
		}, {
			[]int{4, 3, 7, 4, 2, 7, 9, 4, 7, 4, 7},
			11,
			[]int{7, 4, 7, 4, 9, 7, 2, 4, 7, 3, 4},
		},
	}
	baseTest(t, DealIntoNewStack, tests)
}

func TestCutN(t *testing.T) {
	tests := []testParams{
		{
			[]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
			3,
			[]int{3, 4, 5, 6, 7, 8, 9, 0, 1, 2},
		},
		{
			[]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
			-4,
			[]int{6, 7, 8, 9, 0, 1, 2, 3, 4, 5},
		},
	}
	baseTest(t, CutN, tests)
}

func TestDealIncrementN(t *testing.T) {
	tests := []testParams{
		{
			[]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
			3,
			[]int{0, 7, 4, 1, 8, 5, 2, 9, 6, 3},
		},
	}
	baseTest(t, DealIncrement, tests)
}

func baseTest(t *testing.T, sut func([]int, int) []int, tests []testParams) {
	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := test.expected
			actual := sut(test.initial, test.n)
			for i := range expected {
				if actual[i] != expected[i] {
					t.Fatalf("expected: '%v', actual '%v'", expected, actual)
				}
			}
		})
	}
}
