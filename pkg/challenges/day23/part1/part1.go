package part1

import (
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/challenges/day23/intcode"
	"runtime"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	runtime.GOMAXPROCS(100)
	var state intcode.State
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	messages := make(chan Message, 1000)

	outChannels := map[int]*chan int{}
	haltChannel := make(chan struct{}, 50)
	var computers []*intcode.Computer
	for i := 0; i < 50; i++ {
		computer := intcode.NewComputer(i, i, state)
		channel := make(chan int, 1000)
		outChannels[i] = &channel
		computer.OutChannel = &channel
		computers = append(computers, &computer)

		go func() {
			listen(&channel, &messages, &haltChannel)
		}()
		go func() {
			computer.Run()
		}()
	}

	for message := range messages {
		if message.address == 255 {
			answer = message.y
			close(messages)
		} else {
			comp := computers[message.address]
			*comp.InChannel <- message.x
			*comp.InChannel <- message.y
		}
	}

	for _, computer := range computers {
		computer.Halt()
		haltChannel <- struct{}{}
	}

	return
}

type Message struct {
	address, x, y int
}

func listen(channel *chan int, output *chan Message, haltChannel *chan struct{}) {
	for {
		var addr, x, y int
		select {
		case addr = <-*channel:
		case <-*haltChannel:
			break
		}
		select {
		case x = <-*channel:
		case <-*haltChannel:
			break
		}
		select {
		case y = <-*channel:
		case <-*haltChannel:
			break
		}
		*output <- Message{
			address: addr,
			x:       x,
			y:       y,
		}
	}
}
