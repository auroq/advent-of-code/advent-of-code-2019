package intcode

import (
	"log"
	"sync"
)

type Program struct {
	Computers     []*Computer
	InitialState  []int
	outputChannel *chan int
	halted        bool
}

func NewProgram(ampPhases []int, initialState []int) Program {
	newProgram := Program{
		Computers:     nil,
		InitialState:  initialState,
		outputChannel: nil,
		halted:        false,
	}
	for i, phase := range ampPhases {
		amp := NewComputer(i, phase, initialState)
		if i > 0 {
			newProgram.Computers[i-1].OutChannel = amp.InChannel
		}
		newProgram.Computers = append(newProgram.Computers, &amp)
	}

	return newProgram
}

func (program *Program) SetFinalOutputConfiguration(channel *chan int) {
	program.outputChannel = channel
	program.Computers[len(program.Computers)-1].setOutChannel(channel)
}

func (program Program) Run() {
	var wg sync.WaitGroup
	for _, amplifier := range program.Computers {
		wg.Add(1)
		go func(amp *Computer) {
			defer wg.Done()
			err := amp.Run()
			if err != nil {
				log.Fatal(err)
			}
		}(amplifier)
	}
	wg.Wait()
}

func (program *Program) Halt() {
	program.halted = true
	for _, computer := range program.Computers {
		computer.Halt()
	}
}

func (program *Program) Close() {
	for _, computer := range program.Computers {
		computer.close()
	}
}
