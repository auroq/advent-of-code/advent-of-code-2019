package intcode

import (
	"strconv"
	"testing"
)

func TestParseOpcode(t *testing.T) {
	var inputs = []struct {
		input          int
		expectedOpcode int
		expectedModes  ModeGroup
	}{
		{1, 1, ModeGroup{PositionMode, PositionMode, PositionMode}},
		{101, 1, ModeGroup{ImmediateMode, PositionMode, PositionMode}},
		{1101, 1, ModeGroup{ImmediateMode, ImmediateMode, PositionMode}},
		{11101, 1, ModeGroup{ImmediateMode, ImmediateMode, ImmediateMode}},
		{11001, 1, ModeGroup{PositionMode, ImmediateMode, ImmediateMode}},
		{10101, 1, ModeGroup{ImmediateMode, PositionMode, ImmediateMode}},
		{10001, 1, ModeGroup{PositionMode, PositionMode, ImmediateMode}},

		{2, 2, ModeGroup{PositionMode, PositionMode, PositionMode}},
		{102, 2, ModeGroup{ImmediateMode, PositionMode, PositionMode}},
		{1102, 2, ModeGroup{ImmediateMode, ImmediateMode, PositionMode}},
		{11102, 2, ModeGroup{ImmediateMode, ImmediateMode, ImmediateMode}},
		{11002, 2, ModeGroup{PositionMode, ImmediateMode, ImmediateMode}},
		{10102, 2, ModeGroup{ImmediateMode, PositionMode, ImmediateMode}},
		{10002, 2, ModeGroup{PositionMode, PositionMode, ImmediateMode}},

		{3, 3, ModeGroup{PositionMode, PositionMode, PositionMode}},
		{103, 3, ModeGroup{ImmediateMode, PositionMode, PositionMode}},
		{1103, 3, ModeGroup{ImmediateMode, ImmediateMode, PositionMode}},
		{11103, 3, ModeGroup{ImmediateMode, ImmediateMode, ImmediateMode}},
		{11003, 3, ModeGroup{PositionMode, ImmediateMode, ImmediateMode}},
		{10103, 3, ModeGroup{ImmediateMode, PositionMode, ImmediateMode}},
		{10003, 3, ModeGroup{PositionMode, PositionMode, ImmediateMode}},

		{4, 4, ModeGroup{PositionMode, PositionMode, PositionMode}},
		{104, 4, ModeGroup{ImmediateMode, PositionMode, PositionMode}},
		{1104, 4, ModeGroup{ImmediateMode, ImmediateMode, PositionMode}},
		{11104, 4, ModeGroup{ImmediateMode, ImmediateMode, ImmediateMode}},
		{11004, 4, ModeGroup{PositionMode, ImmediateMode, ImmediateMode}},
		{10104, 4, ModeGroup{ImmediateMode, PositionMode, ImmediateMode}},
		{10004, 4, ModeGroup{PositionMode, PositionMode, ImmediateMode}},
	}

	for _, input := range inputs {
		t.Run(strconv.Itoa(input.input), func(t *testing.T) {
			opcode, err := parseOpcode(input.input)

			if err != nil {
				t.Error(err)
			}

			actualOpcode := opcode.code
			actualModes := opcode.modeGroup

			if actualOpcode != input.expectedOpcode {
				t.Errorf("expected: %d, actual: %d", input.expectedOpcode, actualOpcode)
			}
			for i, act := range actualModes {
				if act != input.expectedModes[i] {
					t.Errorf("expected: %d, actual: %d", input.expectedModes, actualModes)
				}
			}
		})
	}
}
