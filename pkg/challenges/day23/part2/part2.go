package part2

import (
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/challenges/day23/intcode"
	"runtime"
	"strings"
	"sync"
	"time"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	runtime.GOMAXPROCS(100)
	var state intcode.State
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	messages := make(chan Message, 1000)

	outChannels := map[int]*chan int{}
	haltChannel := make(chan struct{}, 50)
	var computers []*intcode.Computer
	for i := 0; i < 50; i++ {
		computer := intcode.NewComputer(i, i, state)
		channel := make(chan int, 1000)
		outChannels[i] = &channel
		computer.OutChannel = &channel
		computers = append(computers, &computer)

		go func() {
			listen(&channel, &messages, &haltChannel)
		}()
		go func() {
			computer.Run()
		}()
	}

	nat := Nat{}

	go func(messages chan Message) {
		for message := range messages {
			if message.address == 255 {
				nat.x = message.x
				nat.y = message.y
			} else {
				comp := computers[message.address]
				*comp.InChannel <- message.x
				*comp.InChannel <- message.y
			}
		}
	}(messages)

	wg := sync.WaitGroup{}
	wg.Add(1)
	ys := map[int]int{}
	go func() {
		for {
			time.Sleep(100 * time.Millisecond)
			allIdle := true
			for _, computer := range computers {
				if !computer.Idle {
					allIdle = false
				}
			}
			if allIdle {
				*computers[0].InChannel <- nat.x
				*computers[0].InChannel <- nat.y
				if _, ok := ys[nat.y]; ok {
					answer = nat.y
					wg.Done()
					break
				} else {
					ys[nat.y] = 1
				}
			}
		}
	}()
	wg.Wait()

	for _, computer := range computers {
		computer.Halt()
		haltChannel <- struct{}{}
	}

	return
}

type Nat struct {
	x, y int
}

type Message struct {
	address, x, y int
}

func listen(channel *chan int, output *chan Message, haltChannel *chan struct{}) {
	for {
		var addr, x, y int
		select {
		case addr = <-*channel:
		case <-*haltChannel:
			break
		}
		select {
		case x = <-*channel:
		case <-*haltChannel:
			break
		}
		select {
		case y = <-*channel:
		case <-*haltChannel:
			break
		}
		*output <- Message{
			address: addr,
			x:       x,
			y:       y,
		}
	}
}
