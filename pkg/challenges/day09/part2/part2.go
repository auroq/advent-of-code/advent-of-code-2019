package part2

import (
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"strings"
	"sync"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	var state intcode.State
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	channel := make(chan int)
	program := intcode.NewProgram([]int{2}, state)
	program.SetFinalOutputConfiguration(&channel)
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		for val := range channel {
			answer = val
		}
		wg.Done()
	}()
	program.Run()
	program.Close()
	close(channel)
	wg.Wait()

	return
}
