package part2

import "errors"

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	err = errors.New("this part is not yet implemented")

	return
}
