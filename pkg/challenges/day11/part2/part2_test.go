package part2

import (
	"strconv"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	t.Skip()
	var input TestState
	input = input.setOutput(1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0).close()
	expected := 6
	actual, err := Part2{}.Run([]string{(string)(input)})
	if err != nil {
		t.Error(err)
	}

	if actual != expected {
		t.Fatalf("expected: '%d', actual: '%d'", expected, actual)
	}
}

type TestState string

func (state TestState) setOutput(vals ...int) TestState {
	var out TestState
	for _, val := range vals {
		out += (TestState)(",104," + strconv.Itoa(val))
	}

	return state + out
}

func (state TestState) setInput(val int) TestState {
	return state + ",103,1000"
}

func (state TestState) close() TestState {
	return state[1:] + (TestState)(",99")
}

func TestRobot_RunInput(t *testing.T) {
	t.Skip()
	// Used for manual testing with input
	var input TestState
	input = input.setInput(0).
		setOutput(1, 0).
		setInput(0).
		setOutput(0, 0, 1, 0, 0, 1).
		setInput(1).
		setOutput(0, 1, 1, 0, 1, 0).
		close()

	var x TestState
	x.setInput(0)

	_, err := Part2{}.Run([]string{(string)(input)})
	if err != nil {
		t.Error(err)
	}
}
