package part1

import (
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"strconv"
	"strings"
)

type Part1 struct{}

type Direction int

const (
	North Direction = iota
	South Direction = iota
	East  Direction = iota
	West  Direction = iota
)

type Position struct {
	row, col int
}

type Robot struct {
	direction     Direction
	position      Position
	grid          *Grid
	program       *intcode.Program
	inputChannel  *chan int
	outputChannel *chan int
	CellsPainted  map[string]int
}

type Cell struct {
	white    bool
	position Position
}

type Grid map[string]*Cell

func (pos Position) ToString() string {
	return strconv.Itoa(pos.row) + "," + strconv.Itoa(pos.col)
}

func (pos Position) Get(direction Direction) Position {
	switch direction {
	case North:
		return Position{row: pos.row - 1, col: pos.col}
	case South:
		return Position{row: pos.row + 1, col: pos.col}
	case East:
		return Position{row: pos.row, col: pos.col + 1}
	case West:
		return Position{row: pos.row, col: pos.col - 1}
	default:
		return pos
	}
}

func (cell *Cell) paint(white bool) {
	cell.white = white
}

func (grid *Grid) paint(position Position, white bool) {
	grid.getCell(position).paint(white)
}

func (grid *Grid) getCell(position Position) *Cell {
	if cell, ok := (*grid)[position.ToString()]; ok {
		return cell
	}
	cell := Cell{
		white:    false,
		position: position,
	}
	(*grid)[position.ToString()] = &cell
	return &cell
}

func (robot *Robot) paint(white bool) {
	robot.grid.paint(robot.position, white)
	robot.CellsPainted[robot.position.ToString()] = 1
}

func NewRobot(state intcode.State) *Robot {
	outChannel := make(chan int, 1)
	program := intcode.NewProgram([]int{0}, state)
	program.SetFinalOutputConfiguration(&outChannel)

	return &Robot{
		direction:     North,
		position:      Position{0, 0},
		grid:          &Grid{},
		program:       &program,
		inputChannel:  program.Computers[0].InChannel,
		outputChannel: &outChannel,
		CellsPainted:  map[string]int{},
	}
}

func (robot *Robot) Run() {
	go func() {
		robot.program.Run()
		close(*robot.outputChannel)
	}()
	first := true
	for out := range *robot.outputChannel {
		if first {
			robot.paint(out == 1)
		} else {
			robot.move(out == 1)
		}
		first = !first
	}
}

func (robot *Robot) move(clockwise bool) {
	robot.rotate(clockwise)
	robot.position = robot.position.Get(robot.direction)
	robot.setInput()
}

func (robot *Robot) setInput() {
	for len(*robot.inputChannel) > 0 {
		<-*robot.inputChannel
	}

	if robot.grid.getCell(robot.position).white {
		*robot.inputChannel <- 1
	} else {
		*robot.inputChannel <- 0
	}
}

func (robot *Robot) rotate(clockwise bool) {
	if clockwise {
		switch robot.direction {
		case North:
			robot.direction = East
		case South:
			robot.direction = West
		case East:
			robot.direction = South
		case West:
			robot.direction = North
		}
	} else {
		switch robot.direction {
		case North:
			robot.direction = West
		case South:
			robot.direction = East
		case East:
			robot.direction = North
		case West:
			robot.direction = South
		}
	}
}

func (part Part1) Run(input []string) (answer int, err error) {
	var state intcode.State
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	robot := NewRobot(state)
	robot.Run()

	answer = len(robot.CellsPainted)

	return
}
