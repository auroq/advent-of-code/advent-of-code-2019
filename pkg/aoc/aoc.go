package aoc

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func getSession() (session string, err error) {
	session = os.Getenv("AOC_SESSION")
	if len(session) <= 0 {
		err = errors.New("AOC_SESSION has not been set")
	}

	return
}

func GetInput(day int) (input []string, err error) {
	session, err := getSession()
	if err != nil {
		return
	}

	url := fmt.Sprintf("https://adventofcode.com/2019/day/%d/input", day)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}

	req.Header.Add("cookie", fmt.Sprintf("session=%s", session))

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	input = strings.Split(string(body), "\n")

	return
}

func Submit(day, part int, answer string) error {
	session, err := getSession()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("https://adventofcode.com/2019/day/%d/answer", day)

	payload := strings.NewReader(fmt.Sprintf("level=%d&answer=%v", part, answer))

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("cookie", fmt.Sprintf("session=%s", session))

	res, _ := http.DefaultClient.Do(req)

	if res != nil {
		defer res.Body.Close()
	}
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

	return nil
}
