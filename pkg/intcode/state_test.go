package intcode

import "testing"

func TestState_GetValueWhenPositionExists(t *testing.T) {
	state := State{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
	index := 4
	expected := state[index]
	actual := state.GetValue(index)
	if actual != expected {
		t.Errorf("expected: %d, actual: %d", expected, actual)
	}
}

func TestState_GetValueWhenPositionDoesNotExist(t *testing.T) {
	state := State{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
	index := 100
	value := state.GetValue(index)

	t.Run("Get value should return 0", func(t *testing.T) {
		expected := 0
		if value != expected {
			t.Errorf("expected: %d, actual: %d", expected, value)
		}
	})

	t.Run("Get value should extend list", func(t *testing.T) {
		expected := index + 1
		if actual := len(state); actual != expected {
			t.Errorf("expected length: %d, actual length: %d", expected, actual)
		}
	})
}

func TestState_SetValueWhenPositionExists(t *testing.T) {
	state := State{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
	index := 4
	expected := 100
	state.SetValue(index, expected)
	actual := state[index]
	if actual != expected {
		t.Errorf("expected: %d, actual: %d", expected, actual)
	}
}

func TestState_SetValueWhenPositionDoesNotExist(t *testing.T) {
	state := State{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
	index := 100
	value := 1000
	state.SetValue(index, value)

	t.Run("Set value should extend list", func(t *testing.T) {
		expected := index + 1
		if actual := len(state); actual != expected {
			t.Errorf("expected length: %d, actual length: %d", expected, actual)
		}
	})

	t.Run("Set value should set value at index", func(t *testing.T) {
		actual := state[index]
		if actual != value {
			t.Errorf("expected: %d, actual: %d", value, actual)
		}
	})
}
