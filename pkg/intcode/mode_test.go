package intcode

import "testing"

func TestMode_ToString(t *testing.T) {
	var tests = []struct {
		mode     Mode
		expected string
	}{
		{PositionMode, "Position"},
		{ImmediateMode, "Immediate"},
		{RelativeMode, "Relative"},
	}
	for _, test := range tests {
		t.Run(test.expected, func(t *testing.T) {
			actual := test.mode.ToString()
			if actual != test.expected {
				t.Fatalf("expected: %s, actual: %s", test.expected, actual)
			}
		})
	}
}

func TestGetMode(t *testing.T) {
	var tests = []struct {
		mode     int
		expected Mode
	}{
		{0, PositionMode},
		{1, ImmediateMode},
		{2, RelativeMode},
	}
	for _, test := range tests {
		t.Run(test.expected.ToString(), func(t *testing.T) {
			actual := GetMode(test.mode)
			if actual != test.expected {
				t.Fatalf("expected: %v, actual: %v", test.expected, actual)
			}
		})
	}
}

func TestMode_GetVal(t *testing.T) {
	var tests = []struct {
		name      string
		mode      Mode
		amplifier *Computer
		expected  int
	}{
		{
			"Position Mode",
			PositionMode,
			&Computer{
				0,
				0,
				[]int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
				3,
				0,
				false,
				false,
				false,
				false,
				nil,
				nil,
				nil,
			},
			3,
		}, {
			"Immediate Mode",
			ImmediateMode,
			&Computer{
				0,
				0,
				[]int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
				3,
				0,
				false,
				false,
				false,
				false,
				nil,
				nil,
				nil,
			},
			6,
		}, {
			"Relative Mode No Base",
			RelativeMode,
			&Computer{
				0,
				0,
				[]int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
				3,
				0,
				false,
				false,
				false,
				false,
				nil,
				nil,
				nil,
			},
			3,
		}, {
			"Relative Mode With Base",
			RelativeMode,
			&Computer{
				0,
				0,
				[]int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
				4,
				3,
				false,
				false,
				false,
				false,
				nil,
				nil,
				nil,
			},
			1,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := test.mode.GetVal(test.amplifier.position, test.amplifier)
			if actual != test.expected {
				t.Fatalf("expected: %d, actual: %d", test.expected, actual)
			}
		})
	}
}

func TestMode_GetValRelativeWithBaseOutsideStateLength(t *testing.T) {
	computer := &Computer{
		0,
		0,
		[]int{109, 19, 204, -34},
		2,
		2019,
		false,
		false,
		false,
		false,
		nil,
		nil,
		nil,
	}
	expected := 1000
	computer.state.SetValue(1985, expected)
	actual := RelativeMode.GetVal(3, computer)
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
