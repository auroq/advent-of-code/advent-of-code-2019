package main

import (
	"flag"
	"fmt"
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/aoc"
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/challenges"
	"log"
)

func main() {
	day, part, submit := parseFlags()

	answer1, answer2, err := run(day, part)
	if err != nil {
		log.Fatal(err)
	}
	output(answer1, answer2)
	if submit {
		err := submitAnswer(day, answer1, answer2)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func parseFlags() (day, part int, submit bool) {
	flag.IntVar(&day, "day", -1, "Day to execute")
	flag.IntVar(&part, "part", 0, "Part of day to execute")
	flag.BoolVar(&submit, "submit", false, "Whether to submit after execution")
	flag.Parse()
	return
}

func run(day, part int) (answer1, answer2 string, err error) {
	days := challenges.GetDays()

	if day < 1 || day > len(days) {
		err = fmt.Errorf("invalid day '%d'", day)
		return
	}
	if part < 0 || part > 2 {
		err = fmt.Errorf("invalid part '%d'", part)
		return
	}

	daySource := days[day-1]
	answer1, answer2, err = daySource.Run(part)

	return
}

func submitAnswer(day int, answer1, answer2 string) error {
	if len(answer1) > 0 {
		err := aoc.Submit(day, 1, answer1)
		if err != nil {
			return err
		}
	}
	if len(answer2) > 0 {
		err := aoc.Submit(day, 2, answer1)
		if err != nil {
			return err
		}
	}

	return nil
}

func output(answer1, answer2 string) {
	if len(answer1) > 0 {
		fmt.Printf("Part 1: %s\n", answer1)
	}
	if len(answer2) > 0 {
		fmt.Printf("Part 2: %s\n", answer2)
	}
}
